import sys
import configparser
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
config = configparser.ConfigParser()
config.read('config.ini')

from datetime import timedelta, datetime

from lib.EtlTimeManager import EtlTimeManager
from lib.GoogleTrends import GoogleTrends
from lib.OdsDfSuperheroeParser import OdsDfSuperheroeParser
from lib.models.CasumoDbOds import CasumoDbOds
from lib.models.ModelOdsSuperheroes import ModelOdsSuperheroes


def parse_save_superheroes_day(df_superheroes, superheroe_name, extraction_day, trans_con):
    """
    :param df_superheroes: pandas.dataframe
    :param superheroe_name:  str
    :param extraction_day: str
    :return:
    """
    ods_parser = OdsDfSuperheroeParser(superheroe_name, extraction_day)
    print('        ...parsing results from Google TRends into ODS DW table ')

    df_trend_superheroe = ods_parser.parse_into_table_format(df_superheroes)

    ods_tb_superheroes = ModelOdsSuperheroes()
    num_rows_exist = ods_tb_superheroes.find_trends_for_day(extraction_day, superheroe_name)
    if num_rows_exist <= 0:
        print('      ^^^ Inserting results from ' + superheroe_name + 'in ' + ods_tb_superheroes.table_name + ' ^^^')
        ods_tb_superheroes.dump_df_table(df_trend_superheroe, trans_con)
        return True
    else:
        print('      xxx Cancelling massive import for ' + superheroe_name +
              ': Looks like there is already rows for date ' + extraction_day +
              ' Rows found = ' + str(num_rows_exist))
        return False


def run_etl_frequency(from_date):
    """
    Main thread of execution"
    :return:
    """
    print('    ')
    print(from_date)
    print('-------- START ETL ' + __file__ + ' ---------')
    print('    ')

    keywords = ['Iron Man', 'the Hulk', 'Thor', 'Captain Marvel', 'Captain America']

    end_date = (datetime.strptime(from_date, "%Y-%m-%d") + timedelta(days=TIME_TRENDS_FREQUENCY)).\
        strftime('%Y-%m-%d')

    print('        ...extracting Google Trends from date ' + from_date + ' until ' + end_date)
    google_trend_api = GoogleTrends(keywords, from_date, end_date)
    df_superheroes = google_trend_api.get_interest_by_region()

    con_casumo = CasumoDbOds.connect_db()
    # trans_con = con_casumo.begin()
    # try:
    for superheroe_name in keywords:
        parse_save_superheroes_day(df_superheroes, superheroe_name, from_date, con_casumo)
    # except:
        # trans_con.rollback()
    # else:
        # con_casumo.commit()

    print('>>>>>>>>> END ETL ' + __name__ + ' <<<<<<<<< ')


def run_etl(start_date):
    """
    Will collect from Google Trends reports on a TIME_TRENDS_FREQUENCY basis
    :param start_date:
    :return:
    """
    end_date = start_date + timedelta(days=TIME_MAX_COLLECTION)
    if end_date > datetime.today().date():
        end_date = datetime.today().date()

    if start_date > datetime.today().date():
        print(' END >>>>>>>>>>>>>>>>>>>>> There is NO MORE DATA to collect. Everything is up to date <<<<<<<<< ')
        sys.exit(0)

    for from_date in EtlTimeManager.daterange(start_date, end_date):
        run_etl_frequency(from_date.strftime("%Y-%m-%d"))

# ------------------------------------


TIME_TRENDS_FREQUENCY = int(config['ETL']['TimeTrendsIntervalFrecuency'])
TIME_MAX_COLLECTION = int(config['ETL']['TimeEtlMaxCollection'])

arguments = sys.argv[1:]
user_arg_date = None
if len(arguments)>=1:
    user_arg_date = arguments[0]
extract_day = EtlTimeManager.resolve_last_time_etl(ModelOdsSuperheroes, user_arg_date)
run_etl(extract_day)
