import sys
import configparser
import os.path
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))
config = configparser.ConfigParser()
config.read('config.ini')

import plotly
plotly.tools.set_credentials_file(username=config['PLOTLY']['Username'], api_key=config['PLOTLY']['Apikey'])
import plotly.plotly as py
import plotly.graph_objs as go

from lib.models.ModelOdsSuperheroes import ModelOdsSuperheroes

df_superheroes_popularity = ModelOdsSuperheroes.find_popularity_superheroes_world()
df_pop_thor = df_superheroes_popularity[(df_superheroes_popularity['superheroe'] == 'Thor')]
df_pop_ironman = df_superheroes_popularity[(df_superheroes_popularity['superheroe'] == 'Iron Man')]
df_pop_hulk = df_superheroes_popularity[(df_superheroes_popularity['superheroe'] == 'the Hulk')]
df_pop_america = df_superheroes_popularity[(df_superheroes_popularity['superheroe'] == 'Captain America')]
df_pop_marvel = df_superheroes_popularity[(df_superheroes_popularity['superheroe'] == 'Captain Marvel')]

data = [go.Scatter(x=df_pop_thor[['time']],
                   y=df_pop_thor[['avg_trend']],
                   name="Thor",
                   line=dict(color='#7F7F7F'),
                   opacity=0.8),
        go.Scatter(x=df_pop_ironman[['time']],
                   y=df_pop_ironman[['avg_trend']],
                   name="Ironman",
                   line=dict(color='#33447F'),
                   opacity=0.8),
        go.Scatter(x=df_pop_hulk[['time']],
                   y=df_pop_hulk[['avg_trend']],
                   name="Hulk",
                   line=dict(color='#BBBF7F'),
                   opacity=0.8),
        go.Scatter(x=df_pop_america[['time']],
                   y=df_pop_america[['avg_trend']],
                   name="Captain America",
                   line=dict(color='#7F7111'),
                   opacity=0.8),
        go.Scatter(x=df_pop_marvel[['time']],
                   y=df_pop_marvel[['avg_trend']],
                   name="Captain Marvel",
                   line=dict(color='#7F7FDD'),
                   opacity=0.8)
        ]
layout = dict(
    title="Earth interest in Avengers over time",
    xaxis=dict(range=[df_pop_thor['time'].iloc[0], df_pop_thor['time'].iloc[-1]])
)

fig = dict(data=data, layout=layout)

py.plot(fig, filename='time-series-simple')
