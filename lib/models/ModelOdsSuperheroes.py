import pandas as pd
from lib.models.CasumoDbOds import CasumoDbOds


class ModelOdsSuperheroes:
    """
    Table = casumo_ods_dw.tb_popularity_superheroe
    """
    __db_con__ = CasumoDbOds.connect_db()
    __db_name__ = 'casumo_ods_dw'
    __table_name__ = 'tb_popularity_superheroe'

    def __init__(self):
        """
        Class constructor
        """
        self.db_con = CasumoDbOds.connect_db()
        self.db_name = self.__db_name__
        self.table_name = self.__table_name__

    @staticmethod
    def get_table_name(self):
        return self.__table_name__

    def dump_df_table(self, df, db_open_connection=None):
        """
        :param df: pd.dataframe
        :param db_open_connection: sqlAlchemy connection
        :return:
        """
        if db_open_connection is None:
            db_open_connection = self.db_con

        return df.to_sql(name=self.table_name, con=db_open_connection, if_exists='append', index=False)

    def find_trends_for_day(self, day, superheroe):
        """
        :param day:  str YYYY-MM-DD
        :param superheroe: str
        :return: int
        :rtype: int
        """
        df_rows = pd.read_sql(
            "SELECT * FROM " + self.db_name + "." + self.table_name +
            " AS tbp WHERE tbp.time = '" + day + "' AND tbp.superheroe = '" + superheroe + "';",
            con=self.db_con)

        return df_rows.shape[0]

    @staticmethod
    def find_popularity_superheroes_world():
        """
        :return: pandas.dataframe
        """
        df_rows = pd.read_sql(
            " SELECT tbs.time, tbs.superheroe, AVG(tbs.popularity) AS avg_trend" +
            " FROM " + ModelOdsSuperheroes.__db_name__ + "." + ModelOdsSuperheroes.__table_name__ + " AS tbs" +
            " GROUP BY tbs.superheroe, tbs.time ORDER BY tbs.time ASC, tbs.superheroe ASC; ",
            con=ModelOdsSuperheroes.__db_con__)
        return df_rows

    @staticmethod
    def lookup_last_time():
        """
        Checks last ETL execution on this Table
        """
        date_last_exec = False
        with ModelOdsSuperheroes.__db_con__.connect() as con:
            rs = con.execute(" SELECT MAX(time) FROM " +
                             ModelOdsSuperheroes.__db_name__ + "." +
                             ModelOdsSuperheroes.__table_name__ + ";")
            if rs:
                date_last_exec = rs.fetchone()[0]
        return date_last_exec

# End class ModelOdsSuperheroes
