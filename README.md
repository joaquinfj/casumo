# Casumo code challenge

---

#### Candidate: Joaquín Forcada Jiménez
#### Date: 2019-February

---

### Description of the challenge, by Casumo

> #### The assignment
>Using any of unofficial APIs for Google trends, retrieve information about interest over
time and by region about Marvel Comics vs DC Comics and persist in local database.
Using the data you persisted:

> ● Compute which five regions were the most interested in both of these publishing houses

> ● Tell us who is the Earth's Most Popular Avenger (Iron Man, the Hulk, Thor, Captain
Marvel or Captain America)

> ● Plot the Earth's interest in those five heroes over time (save the image in a file)


> ##### Notes:
> ● We suggest Python API solution of General Mills but you are free to use any approach
you like

> ● MySQL is recommended for local storage

Please see [PDF attached](Marketing-Data-Engineer-Coding-challenge.pdf) in the repository for more details.

***

## Installation
* Requirements **Python 3.6**, **pipenv**, **Mysql**
* To install dependencies you can do:
    * Via **pipenv and PipFile**: please check **Pipfile** in the root directory if you want to find out about dependencies. You can install via:
        > pipenv install
    * Via Pip: 
        > pip install -r requirements.txt
* Please check and customize your configuration values in **./config.ini**:

**config.ini :**
```
[MYSQL]
Host = 127.0.0.1
Port = 8889
User = root
Password = root
DbOdsDatawarehouse = casumo_ods_dw
DbMdDatawarehouse = casumo_md_dw 

[ETL]
TimeTrendsIntervalFrecuency = 1
TimeEtlMaxCollection = 10

[PLOTLY]
Username = joaquin.forcada
Apikey = ************
```

* The database is placed with SQL scripts under the folder **sql-dw/**. Please I recommend using command line to bin/mysql to restore the databases. 
** __You have more information__ on [Restore Mysql README.md](sql-dw/README.md) **



### Why this implementation?
I was doubting to implement the code challenge with Python3.6 + PostgresSQL + Luigi (Tasks/Sceduler/ETLs) +  SqlAlchemy. However, I have left Luigi and PostgreSQL out of the test for the sake of simplicity and because you asked me for MySQL.

Also, for the API of Google Trends I used the recommended GeneralMills-pyTrends. I have added a wrapper on top of it in case the Api client should be replaced. So there is full decoupling between ETLs and the source of extraction.

The whole test is designed with the idea that these tasks will be placed in a **Jenkins** or any other scheduler that is going to be run on a time basis. 

I **would add a Jenkins server and I would add an external Logger service**, maybe ELK or something simpler. Both would help to extract metrics of every task and different steps within each task.

Also, it would be worth it to consider tools like Pentaho PDI or Talend, even when it comes to flexibility they are a pain in...

#### Consideration regarding Google Trends
For the calculations I have NOT ignored the 0 values. The Google Trends documentation **states that 0 values means there is no enough information to evaluate**. I was doubting if replace by NULL values all 0's for calculating the averages and maximums. In general, the trend estimation is a bit confusing for me.

### Run the scripts
There are 3 different tasks: **etl_trends_extract_publishers_daily.py**, **etl_trends_extract_superheroes_daily.py** and **plot_trends_superheroes_df.py**. In order to run the extraction of Publishing houses and store them into the database,
 you can run them as follows:

* Publishing houses:
> $  pipenv run python ./tasks/etl_trends_extract_publishers_daily.py

or

> $  python3.6 ./tasks/etl_trends_extract_publishers_daily.py

By default, if you don't add arguments to the call, it will start collecting 30 days ago.
 However, you can add:
> $   pipenv run python ./tasks/etl_trends_extract_publishers_daily.py 2019-02-01


* Avengers:

> $ python3.6 ./tasks/etl_trends_extract_superheroes_daily.py

or

> $ pipenv run python tasks/etl_trends_extract_superheroes_daily.py 2019-02-01

* Plotting:

> $  pipenv run python tasks/plot_trends_superheroes_df.py
***

## Solutions
* ##### Five regions were the most interested in both of these publishing houses:

```sql
 SELECT tbp.country, tbp.publisher, AVG(tbp.popularity) AS avg_trend, COUNT(tbp.publisher) AS days_trend
  FROM tb_popularity_publisher AS tbp
  WHERE tbp.publisher = 'Marvel Comics'
    /* AND tbp.time BETWEEN DATE('2019-01-01') AND DATE('2019-05-01') */
  GROUP BY tbp.country
  ORDER BY avg_trend DESC
  LIMIT 5;

/* ------ */

SELECT tbp.country, tbp.publisher, AVG(tbp.popularity) AS avg_trend, COUNT(tbp.publisher) AS days_trend
  FROM tb_popularity_publisher AS tbp
  WHERE tbp.publisher = 'DC Comics'
    /* AND tbp.time BETWEEN DATE('2019-01-01') AND DATE('2019-05-01') */
  GROUP BY tbp.country
  ORDER BY avg_trend DESC
  LIMIT 5;
```
Table with results (check [Mysql database](sql-dw/README.md) for more info):

| country     | publisher     | avg_trend          | days_trend |
|-------------|---------------|--------------------|------------|
| India       | Marvel Comics | 47.96666666666667  | 30         |
| Venezuela   | Marvel Comics | 45.53333333333333  | 30         |
| Philippines | Marvel Comics | 43.666666666666664 | 30         |
| Bangladesh  | Marvel Comics | 42.93333333333333  | 30         |
| Spain       | Marvel Comics | 42.833333333333336 | 30         |

| country     | publisher | avg_trend         | days_trend |
|-------------|-----------|-------------------|------------|
| Brazil      | DC Comics | 76.4              | 30         |
| Belgium     | DC Comics | 71.9              | 30         |
| France      | DC Comics | 71.83333333333333 | 30         |
| Netherlands | DC Comics | 70.43333333333334 | 30         |
| Costa Rica  | DC Comics | 69.03333333333333 | 30         |


***
* #####  Most Popular Avenger:

```sql
SELECT tbs.superheroe,
       AVG(tbs.popularity) AS avg_trend,
       COUNT(DISTINCT tbs.time) AS days_count,
       COUNT(DISTINCT tbs.country) AS countries_count

 FROM casumo_ods_dw.tb_popularity_superheroe AS tbs
 GROUP BY tbs.superheroe
 ORDER BY avg_trend DESC
 LIMIT 1;
```

***
* Table with results:

| superheroe | avg_trend | days_count | countries_count |
|------------|-----------|------------|-----------------|
| Thor       | 7.6456    | 30         | 250             |


*** 
* ##### Plot interest over 5 superheroes:
I was about to do a Jupyter Notebook, but I am not sure if you are interested in a ascript that creates charts in PNG format and sends them, or what exactly. 
Nevertheless, I have created onw with **PLOTLY**:

> $ pipenv run python tasks/plot_trends_superheroes_df.py

This will summarize in a chart in Plotly with all information on the database regarding Avengers:


Take a look at the directory **results/charts** within this repository:


![Check plotly URL](results/charts/time-series-simple.png "Interest in Earth on Avengers")

Link => [Plotly Chart published](https://plot.ly/~joaquin.forcada/4/earth-interest-in-avengers-over-time/#/)


