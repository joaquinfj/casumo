### Casumo code challenge - MySQL DUMP

#### Installation: restoring Datawarehouse
> Requirements: Mysql

Restore with mysql command line. The backup is complete and done with **mysqldump**.
> $:\>    /bin/mysql casumo_ods_db  <  sql-dw/casumo_ods_dw.sql

> $:\>    /bin/mysql casumo_ods_dw  <  sql-dw/casumo_multidimensional_dw.sql

The expected name of the databases are as shown above(there are two of them).

The idea was to create 2 databases, just to follow the principles of CIF. One operational data store, and other one snowflake schema.
However, due to lack of time I have just left the **Multidimensional datawarehouse as a proof of concept**. The idea was to apply OLAP to that second database.


Operational Data Store
> sql-dw/casumo_ods_dw.sql >> [casumo_ods_dw]

Multidimensional database
> sql-dw/casumo_md_dw.sql >> [casumo_md_dw]

##### Proposal of ODS
![Diagram of DW](results/imgs/casumo_ods_dw.png)

##### Proposal of multidimensional DW
![Diagram of DW](results/imgs/casumo_md_dw.png)





